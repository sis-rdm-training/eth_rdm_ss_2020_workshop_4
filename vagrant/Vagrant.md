# Vagrant

## 1. Instalation

1. install VirtualBox 6.0.22 from https://www.virtualbox.org/wiki/Download_Old_Builds_6_0
2. install Vagrant 2.2.9 from https://www.vagrantup.com/downloads

## 2. Build / Start / Restart / Up the virtual machine

1. create a working folder and copy the [Vagrantfile](./Vagrantfile)
2. using the command line interface (PowerShell for Windows, a terminal for Linux or macOS) build the virtual machine by running `$ vagrant up` in the new creating working folder. The entire process might take longer than 10 min
    - a new folder `\vagrant_data` is built in the same working folder that is also mounted inside the virtual machine as `\vagrant_data`

## 3. Connect / Ssh to the virtual machine

1. go to the working folder and run `$ vagrant ssh` (the current working directory inside the virtual machine is the new folder `\vagrant_data`)
2. exit running `$ exit`

### 3.1. Run Jupyter Notebook in the virtual machine

1. make sure that you don't have any running jupyter notebook on port 8888, e.g. open in the browser [http://localhost:8888/](http://localhost:8888/) and you should get an error 
2. go to the working folder and connect to the machine with `$ vagrant ssh`
3. run in the virtual machine `$ jupyter notebook --ip='*' --no-browser  --NotebookApp.token='' ` 
4. open in the browser [http://localhost:8888/](http://localhost:8888/)
5. You can close the Jupyter Notebook by pressing in the browser `Quit` or `Ctrl+C` in the terminal where you run `jupyter notebook ...`

It is not possible to run more than one Jupyter Notebook application. 

## 3. Shut down / Halt the virtual machine

1. go to the working folder and run `$ vagrant halt`

## 4. Destroy the virtual machine

1. go to the working folder and run `$ vagrant destroy`