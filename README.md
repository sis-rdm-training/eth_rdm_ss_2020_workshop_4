# ETH Research Data Management Summer School 2021
## Workshop 4 - Reproducible Computational Environments for Data Analysis

### Agenda

1. Intro - HL
2. Computational Notebooks and JupyterLab - HL
(0:00-0:45)

3. Reproducible Environments and Conda - AP
Break
4. Containerization and Docker - AP
(0:45-2:15)

5. Reproducible computing platforms - HL
6. Wrap up - HL
(2:15-3:00)




# ETH Research Data Management Summer School 2020
## Workshop 4 - Reproducible Computational Environments for Data Analysis

The goal of this workshop is to give participants conceptual and practical knowledge on the reproducibility of data analysis with a strong focus on the computational environment. A hands-on introduction to key technologies such as, Jupyter Notebooks, conda, and containerization will be provided.

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.ethz.ch%2Fplamadaa%2Feth_rdm_ss_2020_workshop_4/master)

### Prerequisites

Please install:
- Anaconda Python 3.7 64-bit version in advance https://docs.continuum.io/anaconda/install/ . Please verify your installation https://docs.continuum.io/anaconda/install/verify-install/ and feel comfortable starting the command line interface.
- Docker https://docs.docker.com/get-docker/ - you will need admin rights for the installation and usage. If you do not have admin rights please coordinate with your IT administrator. Please verify the installation by running in a terminal / PowerShell "sudo docker run hello-world”.

## [License](./LICENSE)

The entire content is distributed under Attribution-NonCommercial-ShareAlike 4.0 International ([CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/)).

